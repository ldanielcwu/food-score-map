var count = 0; 

/** Removes all markers, rectangles, and markerClusterers from the map. */
function removeAllOverlays(toggle) {
    
    var len = markersArray.length; 
    
    for (var i = 0; i<len; i++) { 
    	markersArray.pop(); 
    } 
    
    if (!toggle) {
  	    rectArray[0].setMap(null);
        rectArray.length = 0;
    } 
    mc.clearMarkers();
}

/** Given an overlay, adds event listeners to it so that clicking or
 * right-clicking on it will cause certain javaScript methods to be invoked. */
function addMapListeners(overlay) {
    google.maps.event.addListener(overlay, 'rightclick', function(e) {
        if (rightClickEnabled) {

            // Get lock on search, released when document is ready
            rightClickEnabled = false;
            searchPoints = 1;
            searchPointsTraversed = 0;
            totalPointsTraversed += 1;
            step = 100/(x*y*searchPoints);

            $("#numPoints").text("Searched across " + totalPointsTraversed + " points");
            $("#rectangleDimensions").text("Total search area is approximately: " + totalPointsTraversed*rectangleDim.toFixed(3) + " sq km");

    	    var toggle = false; 
            infowindow.close();
            searchResults = [];
            var tempUTMcoor = (new LatLngObj(e.latLng.lat(), e.latLng.lng())).toUTMRef();
            var easting = tempUTMcoor.easting - halfPoints * eastSeparation;
            var northing = tempUTMcoor.northing - halfPoints * northSeparation;
            var tempcoor = (new UTMRef(easting, northing, tempUTMcoor.latZone, tempUTMcoor.lngZone)).toLatLng();
            //var newHomeLat = e.latLng.lat() - halfPoints * latSeparation;
            //var newHomeLng = e.latLng.lng() - halfPoints * lngSeparation;
            pointArray[0][0] = new google.maps.LatLng(tempcoor.lat, tempcoor.lng);
            progress = 0;
            count++;
            heatmapExists = false;
            reService();
            drawViewingWindow(false);
      	    deepLink();
      	    mapSearch();
            
      	    if (heatOff == false) { 
      		    //	heatmap.setMap(null); 
      		    clusterTrigger(); 
      	    }
        }            
    });
    google.maps.event.addListener(overlay, 'click', function() {
        //alert("called addMapListeners - click function");
        dragging = false;
    });
    
    
}

/* 
 * When the user changes the bounds of the rectangle, it sets up a 
 * new pointArray and removes the markers that are no longer in the 
 * bounds of the rectangle 
 */ 

function addBoundListeners(rect, map) { // want to add listener (listens for changes) to rectangle.

    //google.maps.event.addListener(rect, 'idle', function() {
    google.maps.event.addListener(rect, 'bounds_changed', function() { // detecting bounds change - when they change, 
        // listener hears & you need up to 
        //alert("called addBoundListeners - bounds_changed");
        var maxSearch = 1000;

        dimensionsNew = rect.getBounds(); 
        var southWest = dimensionsNew.getSouthWest(); 
        var northEast = dimensionsNew.getNorthEast();

        var southWestLatLng = new LatLngObj(southWest.lat(), southWest.lng());
        var northEastLatLng = new LatLngObj(northEast.lat(), northEast.lng());
        var southWestUTM = southWestLatLng.toUTMRef();
        var northEastUTM = northEastLatLng.toUTMRef();
        
        smallRectSouthWest = southWest;
        smallRectNorthEast = northEast; 

        var isSameNE = false;
        var isSameSW = false; 

        var isTooBig;
        
        

        var sameZone = false;

        if ((northEastUTM.latZone == southWestUTM.latZone) && (northEastUTM.lngZone == southWestUTM.lngZone)) {
            sameZone = true;
        }

        if (sameZone) {     // you can compare by UTM coord
            var eastMeterDiff = northEastUTM.easting - southWestUTM.easting;
            var northMeterDiff = northEastUTM.northing - southWestUTM.northing;

            isTooBig = eastMeterDiff > maxSearch || northMeterDiff > maxSearch;
        } else {        // otherwise, compare using the latlng coord
            var comparison = new LatLngObj(northEastLatLng.lat, southWestLatLng.lng)
            var widthDist = southWestLatLng.LatLngDistance(comparison.lat, comparison.lng);
            var heightDist = northEastLatLng.LatLngDistance(comparison.lat, comparison.lng); 
            
            isTooBig = widthDist > maxSearch || heightDist > maxSearch; 
        }

        if (isTooBig) { // if too big for set dimensions, want to resize it to fit
            // creates new 1km x 1km region from the southWest corner of area you were searching.
            // current implementation: if either rectangle too long or too high or both, recreates 1km x 1km rect.
            removeAllOverlays(false); 
            alert("We only allow for a search region of 1.0kmx1.0km. Please contact us at seto@berkeley.edu to search at bigger areas"); 

            var newNorthEastCoor = (new UTMRef(southWestUTM.easting + 1000, southWestUTM.northing + 1000, southWestUTM.latZone, southWestUTM.lngZone)).toLatLng();
            dimensionsNew = new google.maps.LatLngBounds(new google.maps.LatLng(southWest.lat(), southWest.lng()), new google.maps.LatLng(newNorthEastCoor.lat, newNorthEastCoor.lng));  
            drawMaxFitRectangle(dimensionsNew);
        }

        newPointArray(dimensionsNew); 
        
        southWest = dimensionsNew.getSouthWest(); 
        northEast = dimensionsNew.getNorthEast();


        var inBounds,
        clear,
        i = markersArray.length - 1;
        while (i >= 0) {  // too small get rid of irrelevant/out of bounds points
            inBounds = markersArray[i].position.lat() > southWest.lat(); 
            inBounds &= markersArray[i].position.lng() > southWest.lng(); 
            inBounds &= markersArray[i].position.lat() < northEast.lat(); 
            inBounds &= markersArray[i].position.lng() < northEast.lng();
            
            if (inBounds == false) { 
                clear = true; 
                markersArray.splice(i, 1); // splice = delete actual data in javascript
                                            // 1st arg = index want to delete
            } 
            i--;
        }
        if (clear) { // if new rectangle is smaller and delete from makers array
            // but graphically order not going to change
            // delete all & redraw graphics w/ new markers array (updated data from for loop right before)
            mc.clearMarkers(); 
            addClusterer();
        } 

        if (heatOff == false) { 
            clusterTrigger(); 
        }
        
        smallRectSouthWest = dimensionsNew.getSouthWest();
        smallRectNorthEast = dimensionsNew.getNorthEast();  

        deepLink(); 


        removeSearchResults(rect); 
        heatmapExists = false; 
        heatmapData = []; 

        mapSearch();
        
    });
    
    // }); 
} 


function drawMaxFitRectangle(dimensions) { // if rectangle too big, draw max bigness possible rectangle instead
    //alert("called drawMaxFitRectangle");
    var northEastPoint = dimensions.getNorthEast(),
    southWestPoint = dimensions.getSouthWest(),
    rectBounds = new google.maps.LatLngBounds(southWestPoint, northEastPoint);
    
    rect = new google.maps.Rectangle({
        bounds: rectBounds,
        map: map,
        fillColor: "#5EB7FF",
        fillOpacity: 0.1,
        strokeColor: "#6E9EDC",
        strokeOpacity: 1.0, 
        editable: true 
    });
    
    rect.setVisible(true);
    
    addBoundListeners(rect);
    addMapListeners(rect);
    rectArray.push(rect);
}






/** Creates a standard Google Maps marker to be displayed on the Google Map,
 * and also adds it to the markerClusterer so that it can be added into a
 * cluster if it is close enough.
 *
 * Clicking on the marker will open a Google Maps Infowindow displaying the
 * name and type of the store that the marker represents. */
function createMarker(place) {
    var referenceCode = place.reference;

    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location,
        title: place.name,
        types: place.types,
        addr: place.vicinity,
        place: place,
        reference: referenceCode,
        idNum: idArray[homeIndex],
        userNum: userArray[homeIndex],
        roundNum: userArray[homeIndex],
        sRegionNum: sRegionArray[homeIndex],
    });
    markersArray.push(marker);
    mc.addMarker(marker);

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setPosition(place.geometry.location);
        var contents = "<form><input type=\"button\" value=\" Print this page \" onclick=\"printInfoWindow()\">  <input type=\"button\" value=\" Save this page \" onclick=\"saveInfoWindow()\"></form>";
        contents += "<div id=\"printArea\"><ul>";
        contents += "<li>";
        contents += "<strong>" + place.name + "</strong>" + "<br />";
        contents += "Address: " + place.vicinity + "<br />";
        contents += "Phone Number: " + place.international_phone_number + "<br />";
        contents += "Types: " + place.types.toString().split(",").join(", ") + "<br />";
        contents += "Coordinates: " + place.geometry.location.lat() + ", " + place.geometry.location.lng();
        contents += "</li>";
        contents += "</ul></div>";
        infowindow.setContent(contents);
        infowindow.open(map);
    });
}



/*
 * Draws a rectangle on the Google Map for the area we search in 
 */ 
function drawBounds(firstTime) {
    //alert("called drawBounds");
	var param = $.bbq.getState(); 
	if (custom == true & firstTime == true) { 	
		smallRectSouthWest = new google.maps.LatLng(param.latSW, param.lngSW);
		smallRectNorthEast = new google.maps.LatLng(param.latNE, param.lngNE);  
	}
	else { 
		smallRectSouthWest = pointArray[0][0];
    	smallRectNorthEast = pointArray[points-1][points-1];
    } 

    var rectBounds = new google.maps.LatLngBounds(smallRectSouthWest, smallRectNorthEast);
    
    rect = new google.maps.Rectangle({
        bounds: rectBounds,
        map: map,
        fillColor: "#5EB7FF",
        fillOpacity: 0.1,
        strokeColor: "#6E9EDC",
        strokeOpacity: 1.0, 
        editable: true 
    });
    
    rect.setVisible(true);
    
    addBoundListeners(rect);
    addMapListeners(rect);
    
    rectArray.push(rect);
}

/*
 * Draws a viewing window on the Google Map for the area we search in 
 */ 
function drawViewingWindow(firstTime) {
    //alert("called drawBounds");
	var param = $.bbq.getState(); 
	if (custom == true & firstTime == true) {
		smallRectSouthWest = new google.maps.LatLng(param.latSW, param.lngSW);
		smallRectNorthEast = new google.maps.LatLng(param.latNE, param.lngNE);  
	}
	else { 
		smallRectSouthWest = pointArray[0][0];
    	smallRectNorthEast = pointArray[points-1][points-1];
    } 

    var rectBounds = new google.maps.LatLngBounds(smallRectSouthWest, smallRectNorthEast);
    
    rect = new google.maps.Rectangle({
        bounds: rectBounds,
        map: map,
        fillColor: "#5EB7FF",
        fillOpacity: 0.1,
        strokeColor: "#6E9EDC",
        strokeOpacity: 1.0, 
        editable: false 
    });
    
    rect.setVisible(true);
    
    addBoundListeners(rect);
    addMapListeners(rect);
    
    rectArray.push(rect);
}

/*
 * Creates a button that allows us to pan back to the center of the current
 * Google Places search on the map.
 *
 * Clicking on this button will immediately cause the map to pan (unless
 * the current map's view is too far, at which the view will just change
 * to show the center of the Google Maps search.
 */
function createReturnButton() {
    var returnButton = document.createElement('div');
    returnButton.style.padding = '5px';

    var controlUI = document.createElement('div');
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Return to Search Area';
    controlUI.style.cursor = 'pointer';
    controlUI.style.backgroundColor = 'white';
    controlUI.style.borderWidth = '2px';
    controlUI.style.borderStyle = 'solid';
    returnButton.appendChild(controlUI);

    var controlText = document.createElement('div');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '12px';
    controlText.style.paddingLeft = '4px';
    controlText.style.paddingRight = '4px';
    controlText.innerHTML = 'Return to<br>Search Area';
    controlUI.appendChild(controlText);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(returnButton);

    google.maps.event.addDomListener(returnButton, 'click', function() {
        //alert("called createReturnButton - click function");
        map.panTo(pointArray[halfPoints][halfPoints]);
    });
}

/* 
 * Measures the text of the text next to the map's options' checkboxes.
 * It then uses these lengths to set the widths of the various <div>
 * layers in the html page.
 */

function adjustOptionsDiv() {
    var inputsChecksWidth = $("#inputChecks").css('width');
   	var marginSearch = ($(window).width() - 1000)/2 + 10; 

    // We subtract by an extra 7 so that the window does not cover the border of the map it is on top of.
    $("#inputsBackground").css('margin-top', 33); 
    $("#inputsBackground").css('margin-left', $(window).width()-marginSearch-(175 + 7)); 
    $("#inputChecks").css('margin-right', marginSearch); 
    $("#progressbar").css('margin-right', marginSearch); 
    $("#resultOptions").css('margin-left', marginSearch-15); 
    $("#resultOptions").css('margin-right', marginSearch); 
    $("#logo").css('margin-right', marginSearch+0); 
    $("#mapTypes").css('margin-left', marginSearch-10); 
    $("#bottomBar").css('margin-left', marginSearch-15); 
    $("#shareLink").css('margin-left', marginSearch+165); 
    $("#citris_logo").css('margin-right', marginSearch+160); 

}

/*
 * Hack created so that dragging the map while the cursor is over a
 * markerClusterer will not allow its "click" eventListener to do anything.
 * 
 * In order to trigger the "click" event on the markerClusterer after this,
 * we only need to move the mouse. 
 */
function dragLockForMarkerClusterer() {
    jQuery(document).ready(
        function(){
            $(document).mousemove(function(e){
                if (!dragging) {
                    draglock = false;
                }
            }); 
        }
    )
}

// Resets the progress bar to 0 
function resetProgressBar() { 
	$(document).ready(function() {
   		$("#progressbar").progressbar({ value: searchPointsTraversed*step });
        $('#progressbarlabel').html("Processing...");
	});
}

/* 
 * Restores which food types were being searched for when the user enters 
 * the site using a deeplink 
 */ 
function restoreChecks() { 
	if (custom == true) {  
        var checks = $.bbq.getState(); 
        if (checks.restaurants == undefined || checks.restaurants == "unchecked"){
        	toggleBoolean2("restaurant", false);    	    
        	$('input[value=restaurant]').attr('checked', false); 
        }
        if (checks.takeout == undefined || checks.takeout == "unchecked") {
        	toggleBoolean2("meal_takeaway", false);    	 
        	$('input[value=meal_takeaway]').attr('checked', false); 
        }
        if (checks.delivery == undefined || checks.delivery == "unchecked" ) {
        	toggleBoolean2("meal_delivery", false);   
        	$('input[value=meal_delivery]').attr('checked', false); 
        }
        if (checks.liquor == undefined || checks.liquor == "unchecked") {
        	toggleBoolean2("liquor_store", false);    	    
        	$('input[value=liquor_store]').attr('checked', false); 
        }
        if (checks.grocery == undefined || checks.grocery == "unchecked") {
        	toggleBoolean2("grocery_or_supermarket", false);    	    
        	$('input[value=grocery_or_supermarket]').attr('checked', false); 
        }
        if (checks.food == undefined || checks.food == "unchecked") {
        	toggleBoolean2("food", false);    	    
        	$('input[value=food]').attr('checked', false); 
        }
        if (checks.convenience == undefined || checks.convenience == "unchecked") {
        	toggleBoolean2("convenience_store", false);    	    
        	$('input[value=convenience_store]').attr('checked', false); 
        }
        if (checks.cafe == undefined || checks.cafe == "unchecked") {
        	toggleBoolean2("cafe", false);    	    
        	$('input[value=cafe]').attr('checked', false); 
        }
        if (checks.bar == undefined || checks.bar == "unchecked") {
        	toggleBoolean2("bar", false);    	    
        	$('input[value=bar]').attr('checked', false); 
        }
        if (checks.bakery == undefined || checks.bakery == "unchecked") {
        	toggleBoolean2("bakery", false);    	    
        	$('input[value=bakery]').attr('checked', false); 
        }

    } 
    
} 

/* 
 *Restores the rectangle that the deeplink searched for when the user enters 
 * the site
 */ 
function restoreRect() { 
	var rect = $.bbq.getState(); 
	if (custom == true) { 
		var dimensionsNew = new google.maps.LatLngBounds(new google.maps.LatLng(rect.latSW, rect.lngSW),
														 new google.maps.LatLng(rect.latNE, rect.lngNE)); 
		newPointArray(dimensionsNew); 
	} 
}

/* Turns off the heat map and triggers the cluster map */ 
function clusterTrigger() { 
    if (clusterExists == false) {
        heatOff = true; 
        mc.addMarkers(markersArray);    
        heatmap.setMap(null); 
        heatmapExists = false; 
        clusterExists = true; 
    }
} 

/* 
 * Removes the irrevalant search results from the resultsArray 
 * that gets inputted into the heat map array 
 */ 
function removeSearchResults(rect) {
    //alert("called removeSearchResults");
    dimensionsNew = rect.getBounds(); 
    var southWest = dimensionsNew.getSouthWest(),
    northEast = dimensionsNew.getNorthEast(),
    inBounds,
    clear,
    i = 0,
    searchLen = searchResults.length;
    smallRectSouthWest = southWest; 
    smallRectNorthEast = northEast; 

    while (i < searchLen) {
        var resultsArray = searchResults[i],
        j = resultsArray.length - 1;
        while (j >= 0) {
            inBounds = resultsArray[j].geometry.location.lat() > southWest.lat(); 
            inBounds &= resultsArray[j].geometry.location.lng() > southWest.lng(); 
            inBounds &= resultsArray[j].geometry.location.lat() < northEast.lat(); 
            inBounds &= resultsArray[j].geometry.location.lng() < northEast.lng(); 
            
            if (inBounds == false) { 
                clear = true; 
                resultsArray.splice(j, 1); 
            }
            j--;
        }
        i++;
    }
}
