/** Creates a <div> layer for the autoComplete box on the top right of our
 * version of Google Maps. It will search over the entire earth, and
 * choosing from the places it suggests will take our search to that area.
 *
 * The search can take in country, city, and major subcity names.*/
function setupAutoComplete() {
    var defaultBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(-90, -180),
        new google.maps.LatLng(90, 180)
    ),
    input = document.getElementById('placeSearch'),
    options = {
        bounds: defaultBounds,
        types: ['geocode']
    };
    autocomplete = new google.maps.places.Autocomplete(input, options);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace(),
        mapCenter = place.geometry.location,
        mapCenterUTM = (new LatLngObj(mapCenter.lat(), mapCenter.lng())).toUTMRef(),
        colEasting = mapCenterUTM.easting - (halfPoints)*eastSeparation,
        colNorthing = mapCenterUTM.northing - (halfPoints)*northSeparation,
        tempLatLngObj = (new UTMRef(colEasting, colNorthing, mapCenterUTM.latZone, mapCenterUTM.lngZone)).toLatLng(),
        tempStart = new google.maps.LatLng(tempLatLngObj.lat, tempLatLngObj.lng);
        map.panTo(mapCenter);
        deepLink(); 
    });

}
