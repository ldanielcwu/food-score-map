// Hides element e of form
function hide(id) {
    var e = document.getElementById(id);
    e.style.display="none";
}

// Shows element e of form
function show(id) {
    var e = document.getElementById(id);
    e.style.display="inline";
}

function toggleOther6() {
    var display = document.getElementById("Other6").style.display;
    if (display == "inline") {
        hide("Other6");
    } else if (display == "none") {
        show("Other6");
    } else {
        hide("Other6");
    }
}

function toggleOther7() {
    var display = document.getElementById("Other7").style.display;
    if (display == "inline") {
        hide("Other7");
    } else if (display == "none") {
        show("Other7");
    } else {
        hide("Other7");
    }
}

function toggleOther8() {
    var display = document.getElementById("Other8").style.display;
    if (display == "inline") {
        hide("Other8");
    } else if (display == "none") {
        show("Other8");
    } else {
        hide("Other8");
    }
}

function toggleOther16() {
    var display = document.getElementById("Other16").style.display;
    if (display == "inline") {
        hide("Other16");
    } else if (display == "none") {
        show("Other16");
    } else {
        hide("Other16");
    }
}

function validate() {
    var resultString = ""

    Android.showToast("starting");
    var initials = document.getElementById("observerInitials").value;
    if (initials == "") {
        Android.showToast("no initials");
        return false;
    } else {
        resultString += "observerInitials=" + initials + " || ";
    }

    var neighborhood = document.getElementById("Q1").value;
    if (neighborhood == "") {
        Android.showToast("no q1");
        return false;
    } else {
        resultString += "Q1=" + neighborhood + " || ";
    }

    var storeName = document.getElementById("Q2").value;
    if (storeName == "") {
        Android.showToast("no q2");
        return false;
    } else {
        resultString += "Q2=" + storeName + " || ";
    }

    var storePhone = document.getElementById("Q3").value;
    if (storePhone == "") {
        Android.showToast("no q3");
        return false;
    } else {
        resultString += "Q3=" + storePhone + " || ";
    }

    var storeAddress = document.getElementById("Q4").value;
    if (storeAddress == "") {
        Android.showToast("no q4");
        return false;
    } else {
        resultString += "Q4=" + storeAddress + " || ";
    }

    var openSlider = document.getElementById("Q5a");
    var openAM = document.getElementById("Q5b1");
    var openPM = document.getElementById("Q5b2");
    var closeSlider = document.getElementById("Q5c");
    var closeAM = document.getElementById("Q5d1");
    var closePM = document.getElementById("Q5d2");
    if (!openAM.checked && !openPM.checked) {
        Android.showToast("no q5 open time");
        return false;
    } else if (!closeAM.checked && !closePM.checked) {
        Android.showToast("no q5 close time");
        return false;
    } else {
        resultString += "Q5=" + openSlider.value;
        if (openAM.checked) {
            resultString += openAM.value;
        } else {
            resultString += openPM.value;
        }
        resultString += "," + closeSlider.value;
        if (closeAM.checked) {
            resultString += closeAM.value;
        } else {
            resultString += closePM.value;
        }
        resultString += " || ";
    }

    resultString += "Q6=";
    var foundCheck = false;
    var q6check1 = document.getElementById("Q6a");
    var q6check2 = document.getElementById("Q6b");
    var q6check3 = document.getElementById("Q6c");
    var q6check4 = document.getElementById("Q6d");
    var q6check5 = document.getElementById("Q6e");
    var q6check6 = document.getElementById("Q6f");
    var q6check7 = document.getElementById("Q6g");
    var q6check8 = document.getElementById("Q6h");
    var q6check9 = document.getElementById("Q6i");
    var q6check10 = document.getElementById("Q6j");
    var q6check11 = document.getElementById("Q6k");
    var q6check12 = document.getElementById("Q6l");
    var q6check13 = document.getElementById("Q6m");
    var q6check14 = document.getElementById("Q6n");
    var q6other = document.getElementById("Other6").value;
    var q6tempData1 = getCheckData(q6check1, foundCheck, resultString);
    var q6tempData2 = getCheckData(q6check2, q6tempData1[0], q6tempData1[1]);
    var q6tempData3 = getCheckData(q6check3, q6tempData2[0], q6tempData2[1]);
    var q6tempData4 = getCheckData(q6check4, q6tempData3[0], q6tempData3[1]);
    var q6tempData5 = getCheckData(q6check5, q6tempData4[0], q6tempData4[1]);
    var q6tempData6 = getCheckData(q6check6, q6tempData5[0], q6tempData5[1]);
    var q6tempData7 = getCheckData(q6check7, q6tempData6[0], q6tempData6[1]);
    var q6tempData8 = getCheckData(q6check8, q6tempData7[0], q6tempData7[1]);
    var q6tempData9 = getCheckData(q6check9, q6tempData8[0], q6tempData8[1]);
    var q6tempData10 = getCheckData(q6check10, q6tempData9[0], q6tempData9[1]);
    var q6tempData11 = getCheckData(q6check11, q6tempData10[0], q6tempData10[1]);
    var q6tempData12 = getCheckData(q6check12, q6tempData11[0], q6tempData11[1]);
    var q6tempData13 = getCheckData(q6check13, q6tempData12[0], q6tempData12[1]);
    var q6tempData14 = getCheckData(q6check14, q6tempData13[0], q6tempData13[1]);
    foundCheck = q6tempData13[0];
    resultString = q6tempData13[1];
    if (q6check14.checked) {
        if (q6other == "") {
            Android.showToast("No description for Other in q6");
            return false;
        }
        if (foundCheck) {
            resultString += ",";
        }
        resultString += q6check14.value + "," + q6other;
        foundCheck = true;
    }
    if (!foundCheck) {
        Android.showToast("no checked boxes in q6");
        return false;
    }
    resultString += " || ";

    resultString += "Q7=";
    var foundCheck2 = false;
    var q7check1 = document.getElementById("Q7a");
    var q7check2 = document.getElementById("Q7b");
    var q7check3 = document.getElementById("Q7c");
    var q7check4 = document.getElementById("Q7d");
    var q7check5 = document.getElementById("Q7e");
    var q7check6 = document.getElementById("Q7f");
    var q7check7 = document.getElementById("Q7g");
    var q7check8 = document.getElementById("Q7h");
    var q7check9 = document.getElementById("Q7i");
    var q7check10 = document.getElementById("Q7j");
    var q7check11 = document.getElementById("Q7k");
    var q7other = document.getElementById("Other7").value;
    var q7tempData1 = getCheckData(q7check1, foundCheck2, resultString);
    var q7tempData2 = getCheckData(q7check2, q7tempData1[0], q7tempData1[1]);
    var q7tempData3 = getCheckData(q7check3, q7tempData2[0], q7tempData2[1]);
    var q7tempData4 = getCheckData(q7check4, q7tempData3[0], q7tempData3[1]);
    var q7tempData5 = getCheckData(q7check5, q7tempData4[0], q7tempData4[1]);
    var q7tempData6 = getCheckData(q7check6, q7tempData5[0], q7tempData5[1]);
    var q7tempData7 = getCheckData(q7check7, q7tempData6[0], q7tempData6[1]);
    var q7tempData8 = getCheckData(q7check8, q7tempData7[0], q7tempData7[1]);
    var q7tempData9 = getCheckData(q7check9, q7tempData8[0], q7tempData8[1]);
    var q7tempData10 = getCheckData(q7check10, q7tempData9[0], q7tempData9[1]);
    foundCheck2 = q7tempData10[0];
    resultString = q7tempData10[1];
    if (q7check11.checked) {
        if (q7other == "") {
            Android.showToast("No description for Other in q7");
            return false;
        }
        if (foundCheck2) {
            resultString += ",";
        }
        resultString += q7check11.value + "," + q7other;
        foundCheck2 = true;
    }
    if (!foundCheck2) {
        Android.showToast("no checked boxes in q7");
        return false;
    }
    resultString += " || ";

    resultString += "Q8=";
    var foundCheck3 = false;
    var q8check1 = document.getElementById("Q8a");
    var q8check2 = document.getElementById("Q8b");
    var q8check3 = document.getElementById("Q8c");
    var q8check4 = document.getElementById("Q8d");
    var q8check5 = document.getElementById("Q8e");
    var q8check6 = document.getElementById("Q8f");
    var q8check7 = document.getElementById("Q8g");
    var q8check8 = document.getElementById("Q8h");
    var q8check9 = document.getElementById("Q8i");
    var q8check10 = document.getElementById("Q8j");
    var q8other = document.getElementById("Other8").value;
    var q8tempData1 = getCheckData(q8check1, foundCheck3, resultString);
    var q8tempData2 = getCheckData(q8check2, q8tempData1[0], q8tempData1[1]);
    var q8tempData3 = getCheckData(q8check3, q8tempData2[0], q8tempData2[1]);
    var q8tempData4 = getCheckData(q8check4, q8tempData3[0], q8tempData3[1]);
    var q8tempData5 = getCheckData(q8check5, q8tempData4[0], q8tempData4[1]);
    var q8tempData6 = getCheckData(q8check6, q8tempData5[0], q8tempData5[1]);
    var q8tempData7 = getCheckData(q8check7, q8tempData6[0], q8tempData6[1]);
    var q8tempData8 = getCheckData(q8check8, q8tempData7[0], q8tempData7[1]);
    var q8tempData9 = getCheckData(q8check9, q8tempData8[0], q8tempData8[1]);
    foundCheck3 = q8tempData9[0];
    resultString = q8tempData9[1];
    if (q8check10.checked) {
        if (q8other == "") {
            Android.showToast("No description for Other in q8");
            return false;
        }
        if (foundCheck3) {
            resultString += ",";
        }
        resultString += q8check10.value + "," + q8other;
        foundCheck3 = true;
    }
    if (!foundCheck3) {
        Android.showToast("no checked boxes in q8");
        return false;
    }
    resultString += " || ";

    var storeSize = document.getElementById("Q9").value;
    var storeSizeMode = document.getElementById("Q9a").value;
    if (storeSizeMode == "") {
        Android.showToast("no Q9 mode");
        return false;
    } else if (storeSize == "") {
        Android.showToast("no Q9");
        return false;
    } else {
        resultString += "Q9=" + storeSizeMode + "," + storeSize + " || ";
    }

    var priceVegetable = document.getElementById("Q10a").value;
    var priceMeat = document.getElementById("Q10b").value;
    var priceOther = document.getElementById("Q10c").value;
    if (priceVegetable == "") {
        Android.showToast("no price for Vegetable in Q10");
        return false;
    } else if (priceMeat == "") {
        Android.showToast("no price for Meat in Q10");
        return false;
    } else if (priceOther == "") {
        Android.showToast("no price for Other in Q10");
        return false;
    } else {
        resultString += "Q10=" + priceVegetable + "," + priceMeat + "," + priceOther + " || ";
    }

    resultString += "Q11=";
    var foundCheck4 = false;
    var q11check1 = document.getElementById("Q11a");
    var q11check2 = document.getElementById("Q11b");
    var q11check3 = document.getElementById("Q11c");
    var q11check4 = document.getElementById("Q11d");
    var q11tempData1 = getCheckData(q11check1, foundCheck4, resultString);
    var q11tempData2 = getCheckData(q11check2, q11tempData1[0], q11tempData1[1]);
    var q11tempData3 = getCheckData(q11check3, q11tempData2[0], q11tempData2[1]);
    var q11tempData4 = getCheckData(q11check4, q11tempData3[0], q11tempData3[1]);
    foundCheck4 = q11tempData4[0];
    resultString = q11tempData4[1];
    if (!foundCheck4) {
        Android.showToast("no checked boxes in q11");
        return false;
    }
    resultString += " || ";

    var radioData1 = get2RadioData("Q12", resultString);
    if (radioData1[0]) {
        resultString = radioData1[1];
    } else {
        return false;
    }

    var radioData2 = get2RadioData("Q13", resultString);
    if (radioData2[0]) {
        resultString = radioData2[1];
    } else {
        return false;
    }

    var radioData3 = get2RadioData("Q14", resultString);
    if (radioData3[0]) {
        resultString = radioData3[1];
    } else {
        return false;
    }

    var radioData4 = get2RadioData("Q15", resultString);
    if (radioData4[0]) {
        resultString = radioData4[1];
    } else {
        return false;
    }

    resultString += "Q16=";
    var foundCheck5 = false;
    var q16check1 = document.getElementById("Q16a");
    var q16check2 = document.getElementById("Q16b");
    var q16check3 = document.getElementById("Q16c");
    var q16check4 = document.getElementById("Q16d");
    var q16check5 = document.getElementById("Q16e");
    var q16other = document.getElementById("Other16").value;
    var q16tempData1 = getCheckData(q16check1, foundCheck5, resultString);
    var q16tempData2 = getCheckData(q16check2, q16tempData1[0], q16tempData1[1]);
    var q16tempData3 = getCheckData(q16check3, q16tempData2[0], q16tempData2[1]);
    var q16tempData4 = getCheckData(q16check4, q16tempData3[0], q16tempData3[1]);
    foundCheck5 = q16tempData4[0];
    resultString = q16tempData4[1];
    if (q16check5.checked) {
        if (q16other == "") {
            Android.showToast("No description for Other in q16");
            return false;
        }
        if (foundCheck5) {
            resultString += ",";
        }
        resultString += q16check5.value + "," + q8other;
        foundCheck5 = true;
    }
    if (!foundCheck5) {
        Android.showToast("no checked boxes in q16");
        return false;
    }
    resultString += " || ";

    var radioData5 = get2RadioData("Q17", resultString);
    if (radioData5[0]) {
        resultString = radioData5[1];
    } else {
        return false;
    }

    var radioData6 = get2RadioData("Q18", resultString);
    if (radioData6[0]) {
        resultString = radioData6[1];
    } else {
        return false;
    }

    var radioData7 = get2RadioData("Q19", resultString);
    if (radioData7[0]) {
        resultString = radioData7[1];
    } else {
        return false;
    }

    var radioData8 = get2RadioData("Q20", resultString);
    if (radioData8[0]) {
        resultString = radioData8[1];
    } else {
        return false;
    }

    resultString += "Q21=";
    var foundCheck6 = false;
    var q21check1 = document.getElementById("Q21a");
    var q21check2 = document.getElementById("Q21b");
    var q21check3 = document.getElementById("Q21c");
    var q21tempData1 = getCheckData(q21check1, foundCheck6, resultString);
    var q21tempData2 = getCheckData(q21check2, q21tempData1[0], q21tempData1[1]);
    var q21tempData3 = getCheckData(q21check3, q21tempData2[0], q21tempData2[1]);
    foundCheck6 = q21tempData3[0];
    resultString = q21tempData3[1];
    if (!foundCheck6) {
        Android.showToast("no checked boxes in q21");
        return false;
    }
    resultString += " || ";

    var radioData9 = get2RadioData("Q22", resultString);
    if (radioData9[0]) {
        resultString = radioData9[1];
    } else {
        return false;
    }

    result = resultString;
    return true;
}

function getCheckData(checkbox, foundCheckBefore, resultString) {
    var foundCheck = foundCheckBefore;
    if (checkbox.checked) {
        if (foundCheckBefore) {
            resultString += ",";
        }
        resultString += checkbox.value;
        foundCheck = true;
    }
    return [foundCheck, resultString];
}

function get2RadioData(question, resultString) {
    var radio1 = document.getElementById(question + "a");
    var radio2 = document.getElementById(question + "b");
    if (!radio1.checked && !radio2.checked) {
        Android.showToast("no checked boxes in " + question);
        return [false, ""];
    } else {
        var newString = resultString + question + "=";
        if (radio1.checked) {
            newString += radio1.value + " || ";
        } else {
            newString += radio2.value + " || ";
        }
    }
    return [true, newString];
}

function cancel() {
    document.getElementById("observerInitials").value = "";
    document.getElementById("Q1").value = "";
    document.getElementById("Q2").value = "";
    document.getElementById("Q3").value = "";
    document.getElementById("Q4").value = "";

    document.getElementById("Q5a").value = "6";
    document.getElementById("Q5b1").checked = false;
    document.getElementById("Q5b2").checked = false;
    document.getElementById("Q5c").value = "6";
    document.getElementById("Q5d1").checked = false;
    document.getElementById("Q5d2").checked = false;

    document.getElementById("Q6a").checked = false;
    document.getElementById("Q6b").checked = false;
    document.getElementById("Q6c").checked = false;
    document.getElementById("Q6d").checked = false;
    document.getElementById("Q6e").checked = false;
    document.getElementById("Q6f").checked = false;
    document.getElementById("Q6g").checked = false;
    document.getElementById("Q6h").checked = false;
    document.getElementById("Q6i").checked = false;
    document.getElementById("Q6j").checked = false;
    document.getElementById("Q6k").checked = false;
    document.getElementById("Q6l").checked = false;
    document.getElementById("Q6m").checked = false;
    document.getElementById("Q6n").checked = false;
    document.getElementById("Other6").value = "";

    document.getElementById("Q7a").checked = false;
    document.getElementById("Q7b").checked = false;
    document.getElementById("Q7c").checked = false;
    document.getElementById("Q7d").checked = false;
    document.getElementById("Q7e").checked = false;
    document.getElementById("Q7f").checked = false;
    document.getElementById("Q7g").checked = false;
    document.getElementById("Q7h").checked = false;
    document.getElementById("Q7i").checked = false;
    document.getElementById("Q7j").checked = false;
    document.getElementById("Q7k").checked = false;
    document.getElementById("Other7").value = "";

    document.getElementById("Q8a").checked = false;
    document.getElementById("Q8b").checked = false;
    document.getElementById("Q8c").checked = false;
    document.getElementById("Q8d").checked = false;
    document.getElementById("Q8e").checked = false;
    document.getElementById("Q8f").checked = false;
    document.getElementById("Q8g").checked = false;
    document.getElementById("Q8h").checked = false;
    document.getElementById("Q8i").checked = false;
    document.getElementById("Q8j").checked = false;
    document.getElementById("Other8").value = "";

    document.getElementById("Q9").value = "";
    document.getElementById("Q9a").value = "請選擇長度單位 Please Choose a Unit of Measurement";

    document.getElementById("Q10a").value = "";
    document.getElementById("Q10b").value = "";
    document.getElementById("Q10c").value = "";
    
    document.getElementById("Q11a").checked = false;
    document.getElementById("Q11b").checked = false;
    document.getElementById("Q11c").checked = false;
    document.getElementById("Q11d").checked = false;

    document.getElementById("Q12a").checked = false;
    document.getElementById("Q12b").checked = false;

    document.getElementById("Q13a").checked = false;
    document.getElementById("Q13b").checked = false;

    document.getElementById("Q14a").checked = false;
    document.getElementById("Q14b").checked = false;

    document.getElementById("Q15a").checked = false;
    document.getElementById("Q15b").checked = false;

    document.getElementById("Q16a").checked = false;
    document.getElementById("Q16b").checked = false;
    document.getElementById("Q16c").checked = false;
    document.getElementById("Q16d").checked = false;
    document.getElementById("Q16e").checked = false;
    document.getElementById("Other16").value = "";

    document.getElementById("Q17a").checked = false;
    document.getElementById("Q17b").checked = false;

    document.getElementById("Q18a").checked = false;
    document.getElementById("Q18b").checked = false;

    document.getElementById("Q19a").checked = false;
    document.getElementById("Q19b").checked = false;

    document.getElementById("Q20a").checked = false;
    document.getElementById("Q20b").checked = false;

    document.getElementById("Q21a").checked = false;
    document.getElementById("Q21b").checked = false;
    document.getElementById("Q21c").checked = false;

    document.getElementById("Q22a").checked = false;
    document.getElementById("Q22b").checked = false;

    window.location.reload();
}

function attemptSubmit() {
    if (validate()) {
        Android.submitToast(result);
    }
}
