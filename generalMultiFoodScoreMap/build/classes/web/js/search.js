var searchArea; 
var finshed = false; 
var heatmapI; 
var heatmapJ; 
var fullResults = []; 

/*
 * Runs a Google Place search over a square grid of length designated
 * in berkeleyRestaurants.html 
 */
function mapSearch() {
    //alert("called mapSearch");
    if (searchArray.length > 0) {
        iSearch = 0;
        jSearch = 0;
        
        var request = {
            location: pointArray[iSearch][jSearch],
            radius: rad,
            types: fullSearchArray 
        };

        heatmapI = iSearch; 
        heatmapJ = jSearch; 
        
        infowindow = new google.maps.InfoWindow();
        service.search(request, callback);
    }

}

/*
 * Reinitializes all points in the searching grid based on the
 * latitude and longitude of the first entry in the grid (assumes that
 * some other function changed that entry's latitude and longitude
 * data entries. 
 */
function reService() {
    var startUTM = (new LatLngObj(pointArray[0][0].lat(), pointArray[0][0].lng())).toUTMRef();
    var startEasting = startUTM.easting;
    var startNorthing = startUTM.northing;
	
	if (pointArray.length < 15) { 
		setupPointArray(); 
	} 
    
    resetProgressBar(); 
	
    for (var i = 0; i < points; i++) {
        for (var j = 0; j < points; j++) {
            // Do not set when both i and j are 0
            if ((j + i) > 0) {
                var newEast = startEasting + j*eastSeparation;
                var newNorth = startNorthing + i*northSeparation;
                var newLatLng = (new UTMRef(newEast, newNorth, startUTM.latZone, startUTM.lngZone)).toLatLng();
                var newPoint = new google.maps.LatLng(newLatLng.lat, newLatLng.lng);
                pointArray[i][j] = newPoint;
                if (i == points-1 && j == points-1) {
                    rectangleDim = (newEast - startEasting) * (newNorth - startNorthing) / (1000 * 1000);
                }
            }
        }
    }    
    
} 



/*
 * Removes search parameters for newly unchecked boxes or adds search
 * parameters for newly checked boxes, and then recalculates results. 
 */
function toggleBoolean(booleanFlag, checked) {
    if (progress > 99.99) { 
        var toggle = true; 

        if (checked) {
            searchArray.push(booleanFlag);
            if (searchArray.length == fullSearchArray.length) {
                $('input[value=all]').attr('checked', true); 
                //  alert("check all: "+ searchArray.length + "   fullSearchArray  " + fullSearchArray.length); 
            }
        } else {
            var parameterLength = searchArray.length;
            for (var i = 0; i < parameterLength; i++) {
                if (searchArray[i] == booleanFlag) {
                    searchArray.splice(i, 1);
                    break; 
                }
            }
            $('input[value=all]').attr('checked', false); 
        }
        
        //resetProgressBar(); 
        deepLink();
        heatmapExists = false; 
        removeAllOverlays(toggle);


        markersArray = []; 

        var resultsLen = fullResults.length,
        searchLen = searchArray.length;
        for (i = 0; i < resultsLen; i++) {
            for (var k = 0; k < searchLen; k++) {
                if (fullResults[i].types.indexOf(searchArray[k]) >= 0 ) {
                    createMarker(fullResults[i]); 
                    break; 
                }
            }
        } 

        $(window).bind('hashchange', function(e) {
            var searchParams= e.getState();
        }); 

    } else { 
        alert("Please wait until search has finished"); 
        var checkBox = document.getElementById(booleanFlag); 

        if (checkBox.checked) {
            checkBox.checked = false; 
        } else { 
            checkBox.checked = true; 
        }


    }
	
    showScore();
    printResult(); 

    
}


/* Same as above, except it does not trigger the deepLink */ 
function toggleBoolean2(booleanFlag, checked) {
	var toggle = true; 
    removeAllOverlays(toggle);
    if (checked) {
        searchArray.push(booleanFlag);
    } else {
        var parameterLength = searchArray.length;
        for (var i = 0; i < parameterLength; i++) {
            if (searchArray[i] == booleanFlag) {
                searchArray.splice(i, 1);
            }
        }
    }
    
    resetProgressBar(); 
    heatmapData = []; 

    $(window).bind('hashchange', function(e) {
        var searchParams= e.getState();
        pointArray[0][0] = new google.maps.LatLng(searchParams.lat, searchParams.lng); 
        searchResults = []; 
        mapSearch(); 

    }); 
    
}

/* Either removes all search parameters, or restores all of the search parameters. */ 
function changeAllBoolean(checked) { 

    if (progress > 99.99) { 
        var toggle = true; 
        removeAllOverlays(toggle); 
        var len = searchArray.length;

        for (var i = 0; i < len; i++) { 
            searchArray.pop(); 
        }

        if ($('input[value=all]').attr('checked')) { 
            $('input[name=storeType]').attr('checked', true); 
            searchArray = ['restaurant', 'meal_takeaway', 'meal_delivery', 'liquor_store', 'grocery_or_supermarket', 'food', 'convenience_store', 'cafe', 'bar', 'bakery']; 
            heatResults = searchResults; 

            var resultsLen = fullResults.length;
            for (i = 0; i< resultsLen; i++) {
                createMarker(fullResults[i]); 
            }

        } else { 
            changed = true; 
            $('input[name=storeType]').attr('checked', false);    
        }

        deepLink(); 
        showScore();
    } else { 
        alert("Please wait until search has finished"); 
        var checkBox = document.getElementById("all"); 

        if (checkBox.checked) {
            checkBox.checked = false; 
        } else { 
            checkBox.checked = true; 
        }

    }
} 


/*
 * Initializes a new Google MarkerClusterer to handle Google Maps
 * marker clustering. This makes drawing out search results much
 * faster than normal (because we add less overlays to the map).
 *
 * A DOM Listener is also added to the clusterer such that clicking
 * on a cluster will open a Google Maps InfoWindow that contains
 * all restaurants in the cluster in the InfoWindow's content. 
 */
function addClusterer() {
    clusterOptions = {
        zoomOnClick: false,
        maxZoom: map.maxZoom - 1
    }
    mc = new MarkerClusterer(map, markersArray, clusterOptions);

    google.maps.event.addDomListener(mc, 'click', function(cluster) {
        if (!draglock) {
            infowindow.setPosition(cluster.getCenter());
            var markersLength = cluster.markers_.length,
            contents = "<form><input type=\"button\" value=\" Print this page \"onclick=\"printInfoWindow()\">  <input type=\"button\" value=\" Save this page \"onclick=\"saveInfoWindow()\"></form>";
            contents += "<div id=\"printArea\"><ul>";

            for (var i = 0; i < markersLength; i++) {
                contents += "<li>";
                contents += "<strong>" + cluster.markers_[i].title + "</strong><br> ";
                contents += "Address: " + cluster.markers_[i].addr + "<br>";
                contents += returnTypeString(cluster.markers_[i].types) + "<br>";
                contents += "Coordinates: " + cluster.markers_[i].position.lat() + ", " + cluster.markers_[i].position.lng();
                contents += "</li>";
            }
            contents += "</ul></div>";
            infowindow.setContent(contents);
            infowindow.open(map);
        }
    });

}

/*
 * Prints the html content of the Google Maps InfoWindow 
 */
function printInfoWindow() {
    var windowUrl = 'about:blank';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
    var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');
    printWindow.document.write($("#printArea").html());
    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();
}

/*
 * Prints the html content of the Google Maps InfoWindow 
 */
function saveInfoWindow() {
    var windowUrl = 'about:blank';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
    var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');
    printWindow.document.write($("#printArea").html());
    printWindow.document.close();
    printWindow.focus();
}

function printMap() { 
	var windowUrl = 'about:blank';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
    var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=1000px,height=450px');
    printWindow.document.write($("#map").html());
    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();
} 

/*
 * Takes in an array of type strings, and filters out terms that are
 * not search parameters. It then concatenates the entries of the array. 
 */
function returnTypeString(typeArray) {
    var i = 0,
    types = typeArray.length,
    parameters = searchArray.length,
    newString = "",
    emptyString = true;
    while (i < types) {
        var typeElement = typeArray[i];
        var found = false;
        for (var j = 0; j < parameters; j++) {
            if (typeElement == searchArray[j]) {
                if (emptyString) {
                    newString += typeArray[i];
                    emptyString = false;
                    break;
                } else {
                    newString += ", " + typeArray[i];
                    break;
                }
            }
        }
        i++;
    }
    return "Types: " + newString;
}

/*
 * Function called upon a place upon its being recognized by the
 * placeRequest() method. It checks each place with pre-registered ones,
 * ensuring that it is not a duplicate. If it isn't, the createMarker()
 * method is invoked, creating a marker for the place on the map.
 */
function typeInSearchArray(result) {
    var exists = false,
    len = searchArray.length;
    for (var i = 0; i < len; i++){
        if (result.types.toString().indexOf(searchArray[i].toString()) >= 0) {
            exists = true;            
        } 
    }
    return exists; 
}


function callback(results, status) {

    var inSearchArray;  
    var usedResults = []; 
    

    if (status == google.maps.places.PlacesServiceStatus.OK) {

        var i = 0,
        len = results.length;
        while (i < len) {
            var alreadyMapped = false;
            // inSearchArray = typeInSearchArray(results[i]); 
            inBounds = results[i].geometry.location.lat() >= smallRectSouthWest.lat(); 
            inBounds = inBounds & results[i].geometry.location.lng() >= smallRectSouthWest.lng(); 
            inBounds = inBounds & results[i].geometry.location.lat() <= smallRectNorthEast.lat(); 
            inBounds = inBounds & results[i].geometry.location.lng() <= smallRectNorthEast.lng(); 

            var j = 0,
            markersLen = markersArray.length;
            while (j < markersLen) {
                if (results[i].name == markersArray[j].title) {
                    if (results[i].geometry.location.lat() == markersArray[j].position.lat()) {
                        if (results[i].geometry.location.lng() == markersArray[j].position.lng()) {
                            alreadyMapped = true;
                        }
                    }
                }
                j++;
            }

            if (inBounds && searchArray.length > 0) {
            	if (heatOff) { 
                    usedResults.push(results[i]); 
            	} 

                if (!alreadyMapped & heatOff) {
                    createMarker(results[i]); 
                    fullResults.push(results[i]); 

                }
            }
         	i++;
        }

        searchResults.push(usedResults); 
        heatResults.push(usedResults); 
        var searchPoint = pointArray[heatmapI][heatmapJ]; 
        heatmapData.push(new google.maps.LatLng(searchPoint.lat(), searchPoint.lng())); 

    }
	
    stillSearching = true;
    var incrementedI = false;
    if (iSearch == (y - 1)) {
        if (jSearch == (x - 1)) {
            stillSearching = false;
            jSearch = 0;
            heatmapJ = jSearch; 
            $(document).ready(function() {
                searchPointsTraversed += 1;
				if (inputPointArray.length > (homeIndex + 1))
                    nextCustomSearch();
                else {
   				    $("#progressbar").progressbar({ value: 100 });
                    $('#progressbarlabel').html("Finished.");
                    rightClickEnabled = true;
                }
		    });
            
            showScore();
            sortName();

        } else {
            jSearch = jSearch + 1;
            heatmapJ = jSearch; 
        }
        iSearch = 0;
    } else {
        iSearch = iSearch + 1;
        heatmapI = iSearch; 
        incrementedI = true;
    }

	progress += step; 

    $(document).ready(function() {
   		$("#progressbar").progressbar({ value: progress});
        
        if (progress  == 100) { 
            $('#progressbarlabel').html("Finished.");
            alert($("#results").text());
        } 
        showScore();
        sortName();

	});
    var print = "",
    searchLen = searchArray.length;
	for (var i = 0; i < searchLen; i++) {
        print += searchArray[i] + "  "; 
    }
    if (changed == true){
        // alert(print); 
    }
    //   alert(print); 
    if (stillSearching) {
        var request = {
            location: pointArray[iSearch][jSearch],
            radius: rad,
            types: searchArray
        };

        
        infowindow = new google.maps.InfoWindow();

        var startTime = new Date().getTime() + 200;
        while (new Date() < startTime) {
        }

        service.search(request, callback);
    }



}

/*
 * Only called once.
 *
 * This method is called in order to create a 2-D array that can hold
 * all points in the grid used to search for restaurants in our version
 * of Google Maps. It also initializes all points in the array based on the
 * coordinates of the center of the map when it loads.
 */
function setupPointArray() {
    //alert("called setupPointArray"); // TESTING
    var coor = new LatLngObj(Home.lat(), Home.lng());

    var utmcoor = coor.toUTMRef();

    var startEasting = utmcoor.easting - halfPoints*eastSeparation;
    var startNorthing = utmcoor.northing - halfPoints*northSeparation;
    for (var i = 0; i < y; i++) {
        var columnArray = [];

        var colNorth = startNorthing + i*northSeparation;
        var tempcoor = (new UTMRef(startEasting, colNorth, utmcoor.latZone, utmcoor.lngZone)).toLatLng();
        var columnStart = new google.maps.LatLng(tempcoor.lat, tempcoor.lng);
        columnArray.push(columnStart);
        for (var j = 1; j < x; j++) {
            var tempEast = startEasting + j*eastSeparation;
            tempcoor = (new UTMRef(tempEast, colNorth, utmcoor.latZone, utmcoor.lngZone)).toLatLng();
            var tempStart = new google.maps.LatLng(tempcoor.lat, tempcoor.lng);
            columnArray.push(tempStart);
            if (i == y-1 && j == x-1) {
                rectangleDim = (tempEast - startEasting) * (colNorth - startNorthing) / (1000 * 1000);

            }
        }
        pointArray.push(columnArray);
    }

} 


/* Sets up the new point array with the points of the search area used to search for restaurants in the rectangle drawn by the user. */
function newPointArray(dimensionsNew) { 
	//alert("called newPointArray");
	resetProgressBar(); 

	var northEast = dimensionsNew.getNorthEast(); 
	var southWest = dimensionsNew.getSouthWest(); 


    var startUTM = (new LatLngObj(northEast.lat(), northEast.lng())).toUTMRef();
    var southWestUTM = (new LatLngObj(southWest.lat(), southWest.lng())).toUTMRef();
    if ((startUTM.latZone == southWestUTM.latZone) && (startUTM.lngZone == southWestUTM.lngZone)) {
        pointArray = []; 
        var startE = startUTM.easting; 
        var startN = startUTM.northing; 
        x = new BigNumber(startUTM.easting + 1).subtract(new BigNumber(southWestUTM.easting)); // add 1 to remove rounding error
        // i.e. if x = 999.9999 --> when divide by sep, # of steps gets rounded down to 9 instead of being 10
        x = Math.ceil(x.divide(eastSeparation)); 

        y = new BigNumber(startUTM.northing + 1).subtract(new BigNumber(southWestUTM.northing));
        y = Math.ceil(y.divide(northSeparation)); 

        if (searchPoints > 0)
            step = 100/(x*y*searchPoints);
        else
            step = 100/(x*y); 

        var content; 
        for (i = 0; i < y; i++) {
            var columnArray = [];
            var colNorth = startN - i*northSeparation;
            var tempcoor = (new UTMRef(startE, colNorth, startUTM.latZone, startUTM.lngZone)).toLatLng();
            var columnStart = new google.maps.LatLng(tempcoor.lat, tempcoor.lng);
            columnArray.push(columnStart);
            for (var j = 0; j < x; j++) {
                var tempEast = (new LatLngObj(columnArray[0].lat(), columnArray[0].lng())).toUTMRef().easting - j*eastSeparation;
                tempcoor = (new UTMRef(tempEast, colNorth, startUTM.latZone, startUTM.lngZone)).toLatLng();
                tempStart = new google.maps.LatLng(tempcoor.lat, tempcoor.lng);
                columnArray.push(tempStart);
                content += tempStart +  "    "; 
                if (i == y-1 && j == x-1) {
                    rectangleDim = (tempEast - startE) * (colNorth - startN) / (1000 * 1000);
                }
            }

            
            pointArray.push(columnArray);
        }


    } else {
        var startLatLng = new LatLngObj(northEast.lat(), northEast.lng());
        var southWestLatLng = new LatLngObj(southWest.lat(), southWest.lng());
        var comparingPt = new LatLngObj(southWest.lat(), northEast.lng());
        var heightSeparation = startLatLng.LatLngDistance(comparingPt);
        var widthSeparation = southWestLatLng.LatLngDistance(comparingPt);
        pointArray = []; 
        x = new BigNumber(widthSeparation);
        x = Math.ceil(x.divide(eastSeparation)); 

        y = new BigNumber(heightSeparation);
        y = Math.ceil(y.divide(northSeparation)); 

        var startE = startUTM.easting; 
        var startN = startUTM.northing; 

        if (searchPoints > 0)
            step = 100/(x*y*searchPoints);
        else
            step = 100/(x*y); 

        var content; 
        for (i = 0; i < y; i++) {
            var columnArray = [];
            var colNorth = startN - i*northSeparation;
            var tempcoor = (new UTMRef(startE, colNorth, startUTM.latZone, startUTM.lngZone)).toLatLng();
            var columnStart = new google.maps.LatLng(tempcoor.lat, tempcoor.lng);
            columnArray.push(columnStart);
            for (var j = 0; j < x; j++) {
                var tempEast = (new LatLngObj(columnArray[0].lat(), columnArray[0].lng())).toUTMRef().easting - j*eastSeparation;
                tempcoor = (new UTMRef(tempEast, colNorth, startUTM.latZone, startUTM.lngZone)).toLatLng();
                tempStart = new google.maps.LatLng(tempcoor.lat, tempcoor.lng);
                columnArray.push(tempStart);
                content += tempStart +  "    ";
                if (i == y-1 && j == x-1) {
                    rectangleDim = (tempEast - startE) * (colNorth - startN) / (1000 * 1000);
                }
            }
        }
    }

}

function nextCustomSearch() {
    homeIndex += 1;
    totalPointsTraversed += 1;

    $("#numPoints").text("Searched across " + totalPointsTraversed + " points");
    $("#rectangleDimensions").text("Total search area is approximately : " + totalPointsTraversed*rectangleDim.toFixed(3) + " sq km");
    var toggle = false;
    if (infowindow != undefined)
        infowindow.close();
    searchResults = [];
    var tempHome = inputPointArray[homeIndex]
    var tempUTMcoor = (new LatLngObj(tempHome.lat(), tempHome.lng())).toUTMRef();
    var easting = tempUTMcoor.easting - halfPoints * eastSeparation;
    var northing = tempUTMcoor.northing - halfPoints * northSeparation;
    var tempcoor = (new UTMRef(easting, northing, tempUTMcoor.latZone, tempUTMcoor.lngZone)).toLatLng();
    pointArray[0][0] = new google.maps.LatLng(tempcoor.lat, tempcoor.lng);
    map.panTo(pointArray[0][0]);
    count++;
    heatmapExists = false;
    reService();
    drawViewingWindow(false);
    deepLink();
    mapSearch();

    if (heatOff == false) {
        clusterTrigger();
    }
}
