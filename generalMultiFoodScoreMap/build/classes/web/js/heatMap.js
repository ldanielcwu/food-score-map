var totalPlacesPerType;
var zScoreArray;
var weightArray;
var avgJ = [];
var stdJ = [];
var totalDiff = [];
var tempNumTypes;


/* Triggers the heat map and clears the markers for the cluster map. It checks to see first
** if a heat map already exists, so that the heat maps are not drawn on top of each other
*/

function heatTrigger() {
	if (heatmapExists == false) {
		// Heat map can only be triggered when the search has finishd, float rounding error so 99.99
		if (progress < 99.99) {
			alert("Please try again when the search has finished. Current progress: " + progress + "%");
		} else {
			initializeHeatMap();

			heatOff = false;
			mc.clearMarkers();
			setupHeatPointArray();
	    	heatmap = new google.maps.visualization.HeatmapLayer({
	 			 data: heatPointArray 
			 });

	    	heatmap.setOptions({radius: 13});
		
			addMapListeners(heatmap);
			heatmap.setMap(map);
			heatmapExists = true;
			clusterExists = false;
		}
	}
}


/* Sets up the points on the heat map using Edmund's weighing method
*/
function initializeHeatMap() {

    for (var i = 0; i<heatResults.length; i++) {
        heatResults[i] = [];
    }


    for (i = 0; i < searchResults.length; i++) {
        var tempArray = searchResults[i];
        for (var j = 0; j < tempArray.length; j++) {
            for (var k = 0; k<searchArray.length; k++) {
                if (searchResults[i][j].types.indexOf(searchArray[k]) >= 0) {
              //  alert("keeping " + i + "  " + j + "  " + k + " length: " + searchArray.length +  "   " +  heatResults[i][heatResults[i].length-1].types);
                    heatResults[i].push(searchResults[i][j]);
               //  alert(searchResults[i][j].types + "  =   " + heatResults[i][heatResults[i].length-1].types);
                    break;
                   
                }
            }



        }
   }

	totalPlacesPerType = new Array(searchArray.length);
	zScoreArray = new Array(searchArray.length);
	weightArray = new Array(heatResults.length);

	for (var i = 0; i<searchArray.length; i++) {
		avgJ.push(0);
		stdJ.push(0);
		totalDiff.push(0);
		totalPlacesPerType[i] = new Array(heatResults.length);
		zScoreArray[i] = new Array(heatResults.length);

	}
	

	for (i = 0; i<heatResults.length; i++) {
		for (j = 0; j < searchArray.length; j++) {
			placeTypeScore(heatResults[i], searchArray[j], i, j);
		}
		weightArray[i] = 0;
	}
	
	debugPrint();
	
	for (i = 0; i<searchArray.length; i++) {
		statsGen(i);
	}

	weightScore();
	scaleWeight();

	//alert("heatmapdata length: " + heatmapData.length + "  searhResults" + heatResults[3].length+ "markersArray" + markersArray.length);

}

/* Sets up the scores for each point in a MVCArray to use
** with Google's heat map API later
*/
function setupHeatPointArray() {
	heatPointArray = new google.maps.MVCArray();
		for (var i = 0; i < heatmapData.length; i++) {
			heatPointArray.push({location:heatmapData[i],
								 weight: 1});
		}
	
	
}


/* Counts how many of the inputted restaurant type is found at a certain point
** and stores it into an array to use to calculate the avg, standard deviation,
** and z-score. 
*/
function placeTypeScore(results, type, indexPoints, indexTypes) {
	tempNumTypes = 0;
	for (var i = 0; i<results.length; i++) {
		if(results[i].types.toString().indexOf(type.toString()) >= 0) {
			tempNumTypes++;
		}

	}

	totalPlacesPerType[indexTypes][indexPoints] = tempNumTypes;

}

// Debugging function to check the value of each point
function debugPrint() {
	var content = "";
	for (var j = 0; j<heatResults.length; j++) {
		for (var i = 0; i<totalPlacesPerType.length; i++) {
			content +=  totalPlacesPerType[i][j] + " ";
		}
		content+= "\n";
	}
}

/* Scales the weighted scores of each point so that it ranges from 0-1
*/
function scaleWeight() {
	var min = weightArray[0];
	var max = weightArray[0];
	var range;

	for (var i = 0; i<weightArray.length; i++) {
		if (min > weightArray[i]) {
			min = weightArray[i];
		}

		if (max < weightArray[i]) {
			max = weightArray[i];
		}
	}

	range = max - min;
	for (i = 0; i<weightArray.length; i++) {
		weightArray[i] = (weightArray[i]-min)/range;
	}


}

/* Finds the average, standard deviation, and finally the z-score of each point on the point for
** the inputted food type
*/
function statsGen(indexOfType) {
	var total = 0;
	var totalPoints = heatResults.length;
	for (var i = 0; i<totalPlacesPerType[indexOfType].length; i++) {
		total += totalPlacesPerType[indexOfType][i];
	}

	avgJ[indexOfType] = total/totalPoints;


	for (var j = 0; j < totalPlacesPerType[indexOfType].length; j++) {
		totalDiff[indexOfType] += Math.pow(avgJ[indexOfType] - totalPlacesPerType[indexOfType][j], 2);
	}
	

	stdJ[indexOfType] = Math.sqrt(totalDiff[indexOfType]/(totalPlacesPerType[indexOfType].length-1));


	for (j = 0; j<totalPlacesPerType[indexOfType].length; j++) {
		if (stdJ[indexOfType] != 0) {
			zScoreArray[indexOfType][j] = (totalPlacesPerType[indexOfType][j] - avgJ[indexOfType])/stdJ[indexOfType];
		} else {
			zScoreArray[indexOfType][j] = 0;
		}
	}	
	
}

/* Adds up all the z-scores for each of the restaurant categories and averages the z-scores
** for the final weight score of each point on the heat map
*/
function weightScore() {	

	for (var i = 0; i < heatResults.length; i++) {
		for (var j = 0; j < searchArray.length; j++) {
			weightArray[i] += zScoreArray[j][i]; 
		}
	}

	for (var k = 0; k < heatResults.length; k++) {
		weightArray[k] = weightArray[k]/searchArray.length;
	}

}



