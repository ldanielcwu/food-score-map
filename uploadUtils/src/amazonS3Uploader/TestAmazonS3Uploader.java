package amazonS3Uploader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.UUID;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;

/**
 * Testing harness for AmazonS3Uploader
 * 
 * @author Lemuel Daniel Wu
 */
public final class TestAmazonS3Uploader {
    private TestAmazonS3Uploader() {
        // do nothing. This enforces static calls to test methods
    }

    public static void main(String[] args) {
        selfTest();
    }

    public static void selfTest() {
        String bucketName = "my-first-s3-bucket-" + UUID.randomUUID();
        String key = "MyObjectKey";

        System.out.println("===========================================");
        System.out.println("Starting test for AmazonS3Uploader");
        System.out.println("===========================================\n");

        try {
            AmazonS3Uploader s3Sample = new AmazonS3Uploader();

            System.out.println("Creating bucket " + bucketName + "\n");
            s3Sample.createBucket(bucketName);

            System.out.println("Listing buckets");
            s3Sample.printBuckets();

            System.out.println("Uploading a new object to bucket: " + bucketName + "\n");
            s3Sample.pushFileToServer(bucketName, key, createSampleFile());

            System.out.println("Downloading an object: " + key + " from bucket: " + bucketName);
            System.out.println(s3Sample.getFileContents(bucketName, key));

            System.out.println("Listing objects in bucket: " + bucketName);
            s3Sample.printBucketContents(bucketName);

            System.out.println("Deleting an object: " + key + " from bucket " + bucketName + "\n");
            s3Sample.deleteFileFromServer(bucketName, key);

            System.out.println("Deleting bucket " + bucketName + "\n");
            s3Sample.deleteBucket(bucketName);

            System.out.println("Success!");
        } catch (AmazonServiceException ase) {
            System.out
                    .println("Caught an AmazonServiceException, which means your request made it "
                            + "to Amazon S3, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out
                    .println("Caught an AmazonClientException, which means the client encountered "
                            + "a serious internal problem while trying to communicate with S3, "
                            + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a temporary file with text data to demonstrate uploading a file
     * to Amazon S3
     * 
     * @return A newly created temporary file with text data.
     * 
     * @throws IOException
     */
    private static File createSampleFile() throws IOException {
        File file = File.createTempFile("aws-java-sdk-", ".txt");
        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.write("01234567890112345678901234\n");
        writer.write("!@#$%^&*()-=[]{};':',.<>/?\n");
        writer.write("01234567890112345678901234\n");
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.close();

        return file;
    }
}