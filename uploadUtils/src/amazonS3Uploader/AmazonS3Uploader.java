package amazonS3Uploader;

/* Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"). You may not
 * use this file except in compliance with the License. A copy of the License is
 * located at
 * 
 * http://aws.amazon.com/apache2.0
 * 
 * or in the "license" file accompanying this file. This file is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License. */
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.FileUtils;

import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/**
 * Interface for uploading data onto Amazon S3 servers.
 * 
 * @author Lemuel Daniel Wu
 */
public class AmazonS3Uploader {
    AmazonS3 s3;

    /**
     * Constructor - sets up a new S3Interface object.
     * 
     * @throws IOException
     */
    public AmazonS3Uploader() throws IOException {
        s3 = new AmazonS3Client(new ClasspathPropertiesFileCredentialsProvider());
        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
        s3.setRegion(usWest2);

        File credentials = new File("res/AwsCredentials.properties");
        File innerCredentials = new File("bin/AwsCredentials.properties");
        FileUtils.copyFile(credentials, innerCredentials);
    }

    /**
     * Creates a new S3 bucket.
     * 
     * @param bucketName The name of the new bucket.
     */
    public void createBucket(String bucketName) {
        s3.createBucket(bucketName);
    }

    /**
     * Prints out the names of all the buckets.
     */
    public void printBuckets() {
        for (Bucket bucket : s3.listBuckets())
            System.out.println(" - " + bucket.getName());
        System.out.println();
    }

    /**
     * Push a file to the server.
     * 
     * @param bucketName The bucket the file will be in.
     * @param file The file for uploading.
     */
    public void pushFileToServer(String bucketName, String fileName) {
        pushFileToServer(bucketName, fileName, new File(fileName));
    }

    /**
     * Push a file to the server.
     * 
     * @param bucketName The bucket the file will be in.
     * @param file The file for uploading.
     */
    public void pushFileToServer(String bucketName, File file) {
        pushFileToServer(bucketName, file.getName(), file);
    }

    /**
     * Push a file to the server.
     * 
     * @param bucketName The bucket the file will be in.
     * @param fileName The new name of the bucket.
     * @param file The file for uploading.
     */
    public void pushFileToServer(String bucketName, String fileName, File file) {
        s3.putObject(new PutObjectRequest(bucketName, fileName, file));
    }

    /**
     * Print the objects in a bucket.
     * 
     * @param bucketName The name of the bucket to print from.
     */
    public void printBucketContents(String bucketName) {
        ObjectListing objectListing = s3.listObjects(new ListObjectsRequest()
                .withBucketName(bucketName));
        for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries())
            System.out.println(" - " + objectSummary.getKey() + "  " + "(size = "
                    + objectSummary.getSize() + ")");
        System.out.println();

    }

    /**
     * Deletes a file from a bucket.
     * 
     * @param bucket The bucket to delete the file from.
     * @param fileName The name of the file to delete.
     */
    public void deleteFileFromServer(String bucket, String fileName) {
        s3.deleteObject(bucket, fileName);
    }

    /**
     * Deletes an entire bucket in the S3 server.
     * 
     * @param bucketName the name of the bucket to delete.
     */
    public void deleteBucket(String bucketName) {
        s3.deleteBucket(bucketName);
    }

    /**
     * Displays the contents of the specified input stream as text.
     * 
     * @param bucketName The name of the bucket to get the file from.
     * @param fileName The name of the file to print.
     * 
     * @throws IOException
     */
    public String getFileContents(String bucketName, String fileName) throws IOException {
        S3Object object = s3.getObject(new GetObjectRequest(bucketName, fileName));
        BufferedReader reader = new BufferedReader(new InputStreamReader(object.getObjectContent()));
        final StringBuilder fileContents = new StringBuilder();
        while (true) {
            String line = reader.readLine();
            if (line == null)
                break;
            fileContents.append(line + "\n");
        }
        return fileContents.toString();
    }

}
