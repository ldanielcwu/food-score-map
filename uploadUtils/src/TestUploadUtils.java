import googleSpreadsheetUploader.TestGoogleSpreadsheetUploader;
import amazonS3Uploader.TestAmazonS3Uploader;

/**
 * Testing harness for all of the uploader classes.
 * 
 * @author danielwu
 */
public final class TestUploadUtils {
    public static void main(String[] args) {
        TestAmazonS3Uploader.selfTest();
        TestGoogleSpreadsheetUploader.selfTest();
        System.out.println("Success!");
    }
}