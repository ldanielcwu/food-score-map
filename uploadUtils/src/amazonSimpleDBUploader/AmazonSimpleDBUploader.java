package amazonSimpleDBUploader;

/* Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"). You may not
 * use this file except in compliance with the License. A copy of the License is
 * located at
 * 
 * http://aws.amazon.com/apache2.0
 * 
 * or in the "license" file accompanying this file. This file is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License. */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatch.model.InvalidParameterValueException;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.AttributeDoesNotExistException;
import com.amazonaws.services.simpledb.model.MissingParameterException;
import com.amazonaws.services.simpledb.model.NoSuchDomainException;
import com.amazonaws.services.simpledb.model.NumberDomainAttributesExceededException;
import com.amazonaws.services.simpledb.model.NumberDomainBytesExceededException;
import com.amazonaws.services.simpledb.model.NumberItemAttributesExceededException;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;

/**
 * Uploader to Amazon Web Service SimpleDB interface
 * 
 * @author Lemuel Daniel Wu, Edmund Seto
 */
public final class AmazonSimpleDBUploader {
    private String domain; // like a database table
    private String itemname; // should be a unique id within the entire SimpleDB
                             // domain

    AmazonSimpleDB sdb;

    private List<String> uploadFields; // these are the headers in a list object

    AmazonSimpleDBUploader(String domain, String itemname, String[] uploadFieldsArray) {
        this.domain = domain;
        this.itemname = itemname;
        this.uploadFields = toArrayList(uploadFieldsArray);

        sdb = new AmazonSimpleDBClient(new ClasspathPropertiesFileCredentialsProvider());
        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
        sdb.setRegion(usWest2);

        try {
            File credentials = new File("res/AwsCredentials.properties");
            File innerCredentials = new File("bin/AwsCredentials.properties");
            FileUtils.copyFile(credentials, innerCredentials);
        } catch (IOException e) {
            System.err
                    .println("Warning: Unable to copy Amazon credentials. Process may not execute normally.");
        }
    }

    private static List<String> toArrayList(String[] array) {
        return new ArrayList<String>(Arrays.asList(array));
    }

    // parse the values out of uploadContents and stick into SimpleDB
    public void uploadAfterParsingString(String uploadContents) {
        String[] contentsArray = uploadContents.split("\\|\\|");
        sendParsedStringToSimpleDB(toArrayList(contentsArray));
    }

    private void sendParsedStringToSimpleDB(List<String> uploadList) {
        try {
            // item creation
            List<ReplaceableAttribute> attrs = new ArrayList<ReplaceableAttribute>(
                    this.uploadFields.size() + 1);

            ReplaceableAttribute AttributeName = new ReplaceableAttribute("itemname", itemname,
                    Boolean.TRUE);
            attrs.add(AttributeName);

            // note: we should pad numbers if you want to sort strings
            // correctly.
            // String paddedScore = SimpleDBUtils.encodeZeroPadding( score, 10
            // ); //Encodes positive float value into a string by zero-padding
            // number up to the specified number of digits so that sorting of
            // string numbers works.

            for (int i = 0; i < uploadFields.size(); i++) {
                ReplaceableAttribute Attribute = new ReplaceableAttribute(uploadFields.get(i),
                        uploadList.get(i), Boolean.TRUE);
                attrs.add(Attribute);
            }

            // item name could be the value of attribute1
            PutAttributesRequest par = new PutAttributesRequest(domain, itemname, attrs);
            sdb.putAttributes(par);

        } catch (InvalidParameterValueException | MissingParameterException pe) {
            System.err.println(pe.getMessage());
        } catch (NumberItemAttributesExceededException | AttributeDoesNotExistException ae) {
            System.err.println(ae.getMessage());
        } catch (NumberDomainBytesExceededException | NumberDomainAttributesExceededException
                | NoSuchDomainException de) {
            System.err.println(de.getMessage());
        } catch (AmazonClientException ce) {
            System.err.println(ce.getMessage());
        }
    }

}
