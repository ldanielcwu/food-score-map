// Hides element e of form
function hide(id) {
    var e = document.getElementById(id);
    e.style.display="none";
}

// Shows element e of form
function show(id) {
    var e = document.getElementById(id);
    e.style.display="inline";
}

function toggleOther6() {
    var display = document.getElementById("Q6Other").style.display;
    if (display == "inline") {
        hide("Q6Other");
    } else if (display == "none") {
        show("Q6Other");
    } else {
        hide("Q6Other");
    }
}

function toggleQ7() {
    var selector = document.getElementById("select7");
    if (selector.value == "长(米) 和 宽(米) Length and Width") {
        show("Q7part1");
        hide("Q7part2");
    } else if (selector.value == "算地板砖 Number of Tiles") {
        hide("Q7part1");
        show("Q7part2");
    } else {
        hide("Q7part1");
        hide("Q7part2");
    }
}

function toggleQ(question) {
    var radioOptions = document.getElementsByName(question + "a");
    var foundCheck = false;
    for (var i = 0; i < radioOptions.length; i += 1) {
        if (radioOptions[i].checked) {
            foundCheck = true;
            if (radioOptions[i].value == "有 Yes") {
                show(question);
            } else {
                hide(question);
            }
            break;
        }
    }
    if (!foundCheck) {
        hide(question);
    }
}

function validate() {
    var resultString = ""

    var initials = document.getElementById("observerInitials").value;
    if (initials == "") {
        Android.showToast("no initials");
        return false;
    } else {
        resultString += "observerInitials=" + initials + " || ";
    }

    var neighborhood = document.getElementById("Q1").value;
    if (neighborhood == "") {
        Android.showToast("Q1 is blank");
        return false;
    } else {
        resultString += "Q1=" + neighborhood + " || ";
    }

    var storeName = document.getElementById("Q2").value;
    if (storeName == "") {
        Android.showToast("Q2 is blank");
        return false;
    } else {
        resultString += "Q2=" + storeName + " || ";
    }

    var storePhone = document.getElementById("Q3").value;
    if (storePhone == "") {
        Android.showToast("Q3 is blank");
        return false;
    } else {
        resultString += "Q3=" + storePhone + " || ";
    }

    var storeAddress = document.getElementById("Q4").value;
    if (storeAddress == "") {
        Android.showToast("Q4 is blank");
        return false;
    } else {
        resultString += "Q4=" + storeAddress + " || ";
    }

    var openSlider = document.getElementById("Q5a");
    var openAM = document.getElementById("Q5b1");
    var openPM = document.getElementById("Q5b2");
    var closeSlider = document.getElementById("Q5c");
    var closeAM = document.getElementById("Q5d1");
    var closePM = document.getElementById("Q5d2");
    if (!openAM.checked && !openPM.checked) {
        Android.showToast("no q5 open time");
        return false;
    } else if (!closeAM.checked && !closePM.checked) {
        Android.showToast("no q5 close time");
        return false;
    } else {
        resultString += "Q5=" + openSlider.value;
        if (openAM.checked) {
            resultString += openAM.value;
        } else {
            resultString += openPM.value;
        }
        resultString += "," + closeSlider.value;
        if (closeAM.checked) {
            resultString += closeAM.value;
        } else {
            resultString += closePM.value;
        }
        resultString += " || ";
    }

    var q6check1 = document.getElementById("Q6a");
    var q6check2 = document.getElementById("Q6b");
    var q6check3 = document.getElementById("Q6c");
    var q6check4 = document.getElementById("Q6d");
    var q6check5 = document.getElementById("Q6e");
    var q6check6 = document.getElementById("Q6f");
    var q6check7 = document.getElementById("Q6g");
    var q6check8 = document.getElementById("Q6h");
    var q6check9 = document.getElementById("Q6i");
    var q6check10 = document.getElementById("Q6j");
    var q6check11 = document.getElementById("Q6k");
    var q6check12 = document.getElementById("Q6l");
    var q6check13 = document.getElementById("Q6m");
    var q6other = document.getElementById("Q6Other").value;
    var foundCheck = false;
    resultString += "Q6=";
    var tempData1 = getCheckData(q6check1, foundCheck, resultString);
    var tempData2 = getCheckData(q6check2, tempData1[0], tempData1[1]);
    var tempData3 = getCheckData(q6check3, tempData2[0], tempData2[1]);
    var tempData4 = getCheckData(q6check4, tempData3[0], tempData3[1]);
    var tempData5 = getCheckData(q6check5, tempData4[0], tempData4[1]);
    var tempData6 = getCheckData(q6check6, tempData5[0], tempData5[1]);
    var tempData7 = getCheckData(q6check7, tempData6[0], tempData6[1]);
    var tempData8 = getCheckData(q6check8, tempData7[0], tempData7[1]);
    var tempData9 = getCheckData(q6check9, tempData8[0], tempData8[1]);
    var tempData10 = getCheckData(q6check10, tempData9[0], tempData9[1]);
    var tempData11 = getCheckData(q6check11, tempData10[0], tempData10[1]);
    var tempData12 = getCheckData(q6check12, tempData11[0], tempData11[1]);
    foundCheck = tempData12[0];
    resultString = tempData12[1];
    if (q6check13.checked) {
        if (q6other == "") {
            Android.showToast("No description for Other in q6");
            return false;
        }
        if (foundCheck) {
            resultString += ",";
        }
        resultString += q6check13.value + "," + q6other;
        foundCheck = true;
    }
    if (!foundCheck) {
        Android.showToast("no checked boxes in q6");
        return false;
    }
    resultString += " || ";

    var question7mode = document.getElementById("select7").value;
    resultString += "Q7=" + question7mode + ",";
    if (question7mode == "請選擇長度單位 Please Choose a Unit of Measurement") {
        Android.showToast("no mode selected for question 7");
        return false;
    } else if (question7mode == "长(米) 和 宽(米) Length and Width") {        
        var storeLength = document.getElementById("Q7a").value;
        var storeWidth = document.getElementById("Q7b").value;
        if (storeLength == "") {
            Android.showToast("no store length specified in question 7");
            return false;
        } else if (storeWidth == "") {
            Android.showToast("no store width specified in question 7");
            return false;
        } else {
            resultString += storeLength + "," + storeWidth + " || ";
        }
    } else if (question7mode == "算地板砖 Number of Tiles") {
        var numTilesLength = document.getElementById("Q7c").value;
        var tileLength = document.getElementById("Q7d").value;
        var numTilesWidth = document.getElementById("Q7e").value;
        var tileWidth = document.getElementById("Q7f").value;
        if (numTilesLength == "") {
            Android.showToast("no number of tiles along store length specified in question 7");
            return false;
        } else if (tileLength == "") {
            Android.showToast("no tile length specified in question 7");
            return false;
        } else if (numTilesWidth == "") {
            Android.showToast("no number of tiles along store width specified in question 7");
            return false;
        } else if (tileWidth == "") {
            Android.showToast("no tile width specified in question 7");
            return false;
        } else {
            resultString += numTilesLength + "," + tileLength + ",";
            resultString += numTilesWidth + "," + tileWidth + " || ";
        }
    }

    var q8check1 = document.getElementById("Q8a");
    var q8check2 = document.getElementById("Q8b");
    var q8check3 = document.getElementById("Q8c");
    var q8check4 = document.getElementById("Q8d");
    var q8check5 = document.getElementById("Q8e");
    var q8check6 = document.getElementById("Q8f");
    var q8check7 = document.getElementById("Q8g");
    var q8check8 = document.getElementById("Q8h");
    var q8check9 = document.getElementById("Q8i");
    var q8check10 = document.getElementById("Q8j");
    var foundCheck2 = false;
    resultString += "Q8=";
    var tempData13 = getCheckData(q8check1, foundCheck2, resultString);
    var tempData14 = getCheckData(q8check2, tempData13[0], tempData13[1]);
    var tempData15 = getCheckData(q8check3, tempData14[0], tempData14[1]);
    var tempData16 = getCheckData(q8check4, tempData15[0], tempData15[1]);
    var tempData17 = getCheckData(q8check5, tempData16[0], tempData16[1]);
    var tempData18 = getCheckData(q8check6, tempData17[0], tempData17[1]);
    var tempData19 = getCheckData(q8check7, tempData18[0], tempData18[1]);
    var tempData20 = getCheckData(q8check8, tempData19[0], tempData19[1]);
    var tempData21 = getCheckData(q8check9, tempData20[0], tempData20[1]);
    var tempData22 = getCheckData(q8check10, tempData21[0], tempData21[1]);
    foundCheck2 = tempData22[0];
    resultString = tempData22[1];
    if (!foundCheck2) {
        Android.showToast("no checked boxes in q8");
        return false;
    }
    resultString += " || ";

    var valid1 = false;
    var valid2 = false;
    if (validateQ("Q9", resultString)[0]) {
        resultString = validateQ("Q9", resultString)[1];
        if (validateQ("Q10", resultString)[0]) {
            resultString = validateQ("Q10", resultString)[1];
            if (validateQ("Q11", resultString)[0]) {
                resultString = validateQ("Q11", resultString)[1];
                if (validateQ("Q12", resultString)[0]) {
                    resultString = validateQ("Q12", resultString)[1];
                    if (validateQ("Q13", resultString)[0]) {
                        resultString = validateQ("Q13", resultString)[1];
                        if (validateQ("Q14", resultString)[0]) {
                            resultString = validateQ("Q14", resultString)[1];
                            if (validateQ("Q15", resultString)[0]) {
                                resultString = validateQ("Q15", resultString)[1];
                                valid1 = true;
                            }
                        }
                    }
                }
            }
        }
    }
    if (valid1 && validateQ("Q16", resultString)[0]) {
        resultString = validateQ("Q16", resultString)[1];
        if (validateQ("Q17", resultString)[0]) {
            resultString = validateQ("Q17", resultString)[1];
            if (validateQ("Q18", resultString)[0]) {
                resultString = validateQ("Q18", resultString)[1];
                if (validateQ("Q19", resultString)[0]) {
                    resultString = validateQ("Q19", resultString)[1];
                    if (validateQ("Q20", resultString)[0]) {
                        resultString = validateQ("Q20", resultString)[1];
                        if (validateQ("Q21", resultString)[0]) {
                            resultString = validateQ("Q21", resultString)[1];
                            if (validateQ("Q22", resultString)[0]) {
                                resultString = validateQ("Q22", resultString)[1];
                                valid2 = true;
                            }
                        }
                    }
                }
            }
        }
    }
    if (!valid1 || !valid2) {
        return false;
    }

    result = resultString;
    return true;
}

function validateQ(question, resultString) {
    resultString += question + "=";
    var check1 = document.getElementById(question + "a1").checked;
    var check2 = document.getElementById(question + "a2").checked;
    if (!check1 && !check2) {
        Android.showToast("no checked boxes for question " + question + "a");
        return [false, ""];
    } else if (check1) {
        resultString += "有 Yes,";
        var check3 = document.getElementById(question + "b1");
        var check4 = document.getElementById(question + "b2");
        var check5 = document.getElementById(question + "c1");
        var check6 = document.getElementById(question + "c2");
        var check7 = document.getElementById(question + "c3");

        if (!check3.checked && !check4.checked) {
            Android.showToast("no checked boxes for question " + question + "b");
            return [false, ""];
        } else if (check3.checked) {
            resultString += check3.value + ",";
        } else {           
            resultString += check4.value + ",";
        }

        var foundCheck3 = false;
        var tempData1 = getCheckData(check5, foundCheck3, resultString);
        var tempData2 = getCheckData(check6, tempData1[0], tempData1[1]);
        var tempData3 = getCheckData(check7, tempData2[0], tempData2[1]);
        foundCheck3 = tempData3[0];
        resultString = tempData3[1];
        if (!foundCheck3) {
            Android.showToast("no checked boxes for question " + question + "c");
            return [false, ""];
        }

    } else {
        resultString += "无 No";
    }

    if (question != "Q22") {
        resultString += " || ";
    }
    return [true, resultString];
}

function getCheckData(checkbox, foundCheckBefore, resultString) {
    var foundCheck = foundCheckBefore;
    if (checkbox.checked) {
        if (foundCheckBefore) {
            resultString += ",";
        }
        resultString += checkbox.value;
        foundCheck = true;
    }
    return [foundCheck, resultString];
}

function attemptSubmit() {
    if (validate()) {
        Android.submitToast(result);
    }
}

function cancel() {
    document.getElementById("observerInitials").value = "";
    document.getElementById("Q1").value = "";
    document.getElementById("Q2").value = "";
    document.getElementById("Q3").value = "";
    document.getElementById("Q4").value = "";

    document.getElementById("Q5a").value = "6";
    document.getElementById("Q5b1").checked = false;
    document.getElementById("Q5b2").checked = false;
    document.getElementById("Q5c").value = "6";
    document.getElementById("Q5d1").checked = false;
    document.getElementById("Q5d2").checked = false;

    document.getElementById("Q6a").checked = false;
    document.getElementById("Q6b").checked = false;
    document.getElementById("Q6c").checked = false;
    document.getElementById("Q6d").checked = false;
    document.getElementById("Q6e").checked = false;
    document.getElementById("Q6f").checked = false;
    document.getElementById("Q6g").checked = false;
    document.getElementById("Q6h").checked = false;
    document.getElementById("Q6i").checked = false;
    document.getElementById("Q6j").checked = false;
    document.getElementById("Q6k").checked = false;
    document.getElementById("Q6l").checked = false;
    document.getElementById("Q6m").checked = false;
    document.getElementById("Q6Other").value = "";

    // buggy with setting choice for input
    document.getElementById("select7").value = "請選擇長度單位 Please Choose a Unit of Measurement";
    document.getElementById("Q7a").value = "";
    document.getElementById("Q7b").value = "";
    document.getElementById("Q7c").value = "";
    document.getElementById("Q7d").value = "";
    document.getElementById("Q7e").value = "";
    document.getElementById("Q7f").value = "";

    document.getElementById("Q8a").checked = false;
    document.getElementById("Q8b").checked = false;
    document.getElementById("Q8c").checked = false;
    document.getElementById("Q8d").checked = false;
    document.getElementById("Q8e").checked = false;
    document.getElementById("Q8f").checked = false;
    document.getElementById("Q8g").checked = false;
    document.getElementById("Q8h").checked = false;
    document.getElementById("Q8i").checked = false;
    document.getElementById("Q8j").checked = false;

    cancelQ("Q9");
    cancelQ("Q10");
    cancelQ("Q11");
    cancelQ("Q12");
    cancelQ("Q13");
    cancelQ("Q14");
    cancelQ("Q15");
    cancelQ("Q16");
    cancelQ("Q17");
    cancelQ("Q18");
    cancelQ("Q19");
    cancelQ("Q20");
    cancelQ("Q21");
    cancelQ("Q22");
    window.location.reload();
}

function cancelQ(question) {
    document.getElementById(question + "a1").checked = false;
    document.getElementById(question + "a2").checked = false;
    document.getElementById(question + "b1").checked = false;
    document.getElementById(question + "b2").checked = false;
    document.getElementById(question + "c1").checked = false;
    document.getElementById(question + "c2").checked = false;
    document.getElementById(question + "c3").checked = false;
}
