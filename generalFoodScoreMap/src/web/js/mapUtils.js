var typeFile = "csv";

function sortType() {
	sortTypes();
	markersArray.sort(sortByType);
	printResult();
}



function sortName() {
	markersArray.sort(sortByName);
	printResult();

}


function printResult() {
    var contents = [],
    i = 0,
    markerLen = markersArray.length;
	while (i < markerLen) {
		contents.push(i+1 + ". <div id='tableseparators'> Name: </div>" + markersArray[i].title + " | ");
		contents.push("<div id='tableseparators'> Address:  </div>");
		contents.push(markersArray[i].addr + " | ");
		contents.push("<div id='tableseparators'> Types: </div>");
		contents.push(markersArray[i].types + " | ");
		contents.push("<div id='tableseparators'> Coordinates: </div>(");
		contents.push(markersArray[i].position.lat() + ", ");
		contents.push(markersArray[i].position.lng().toString().substring(0,10) + ") <br>");
        i++;
	}
	var contentStr = contents.join("");

	$('#results').html(contentStr);
	$('#results').css('border', '1px solid');

	if (25*markerLen > 300)
		$('#results').css('height', 300);
	else
		$('#results').css('height', 25*markerLen);
}

function sortByName(a, b) {
	var nameA = a.title.toLowerCase();
	var nameB = b.title.toLowerCase();
	if (nameA < nameB) {
		return -1;
	} else if (nameA > nameB) {
		return 1;
	} else {
		return 0;
	}

}

function sortByType(a,b) {
	var typeA = a.types.toString().toLowerCase(),
	typeB = b.types.toString().toLowerCase();
	if (typeA < typeB) {
		return -1;
	} else if (typeA > typeB) {
		return 1;
	} else {
		return 0;
	}
}


function sortTypes() {
	var temp,
    i = 0,
    markerLen = markersArray.length;
	while (i < markerLen) {
		if (markersArray[i].types.toString().indexOf(",") > 0)
			temp = markersArray[i].types.toString().split(",");
		else
			temp = markersArray[i].types.toString().split(";");
		temp.sort();
		temp.join(";");

		markersArray[i].types = temp;
        i++;
	}
}

/* Provides the current state of the search (the search parameters, the rectangle, location, etc.) and
   deep links it so that the user can share the link with others */

function deepLink() {
	var rest;
	var take;
	var liq;
	var del;
	var gro;
	var foo;
	var con;
	var caf;
	var ba;
	var bak;

	if ($("#restaurant").attr('checked') == undefined)
        rest= "unchecked";
    else
        rest= $("#restaurant").attr('checked');

    if ($('input[value=meal_takeaway]').attr('checked') == undefined)
        take= "unchecked";
    else
        take= $('input[value=meal_takeaway]').attr('checked');

    if ($('input[value=meal_delivery]').attr('checked') == undefined)
        del= "unchecked";
    else
        del= $('input[value=meal_delivery]').attr('checked');

    if ($('input[id=liquor_store]').attr('checked') == undefined)
        liq= "unchecked";
    else
        liq= $('input[id=liquor_store]').attr('checked');

    if ($('input[id=grocery_or_supermarket]').attr('checked') == undefined)
        gro= "unchecked";
    else
        gro= $('input[id=grocery_or_supermarket]').attr('checked');

    if ($('input[id=food]').attr('checked') == undefined)
        foo= "unchecked";
    else
        foo= $('input[id=food]').attr('checked');

    if ($('input[id=convenience_store]').attr('checked') == undefined)
        con= "unchecked";
    else
        con= $('input[id=convenience_store]').attr('checked');

    if ($('input[id=cafe]').attr('checked') == undefined)
        caf= "unchecked";
    else
        caf= $('input[id=cafe]').attr('checked');

    if ($('input[id=bar]').attr('checked') == undefined)
        ba= "unchecked";
    else
        ba= $('input[id=bar]').attr('checked');

    if ($('input[id=bakery]').attr('checked') == undefined)
        bak= "unchecked";
    else
        bak= $('input[id=bakery]').attr('checked');

	$.bbq.pushState({
        lat: pointArray[0][0].lat(),
        lng: pointArray[0][0].lng(),
        latSW: smallRectSouthWest.lat(),
        lngSW: smallRectSouthWest.lng(),
        latNE: smallRectNorthEast.lat(),
        lngNE: smallRectNorthEast.lng(),
        restaurants: rest,
        takeout: take,
        delivery: del,
        liquor: liq,
        grocery: gro,
        food: foo,
        convenience: con,
        cafe: caf,
        bar: ba,
        bakery: bak

    });

}

/* Button that generates the link for users to share with others
 */
function shareLink() {
	deepLink();
	var url = document.URL;
	var contentStr = "<button id=\"closeLink\" onclick=\"closeLink()\"><label style=\"font-family:'Montserrat', sans-serif\">x</label></button>";
	contentStr += "<div id=\"url\">" + url + "</div>";
	$('#shareLink').html(contentStr);
	$('#shareLink').show();
}


function closeLink() {
	$("#shareLink").hide();
}

